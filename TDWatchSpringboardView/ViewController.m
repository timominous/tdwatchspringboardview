//
//  ViewController.m
//  Watch
//
//  Created by timominous on 11/12/14.
//  Copyright (c) 2014 fufulabs. All rights reserved.
//

#import "ViewController.h"

#import "TDWatchSpringboardView.h"
#import "TDWatchSpringboardItemView.h"

@interface ViewController ()

@property (strong, nonatomic) TDWatchSpringboardView *springboard;

@end

@implementation ViewController

- (instancetype)init {
  if ((self = [super init]))
    [self onInit];
  return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  if ((self = [super initWithCoder:aDecoder]))
    [self onInit];
  return self;
}

- (void)onInit {
  
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
  if (!_springboard) {
    _springboard = [TDWatchSpringboardView.alloc initWithFrame:self.view.bounds];
    _springboard.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _springboard.backgroundColor = UIColor.lightGrayColor;
    
    NSMutableArray *itemViews = [NSMutableArray array];
    
    for (NSUInteger i = 0; i < 25; i++) {
      UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"image%lu", (i % 6) + 1]];
      NSString *title = [NSString stringWithFormat:@"Badge: %lu", i + 1];
      TDWatchSpringboardItemView *item = [TDWatchSpringboardItemView.alloc initWithTitle:title image:image];
      [itemViews addObject:item];
    }
    
    _springboard.itemViews = itemViews;
    [self.view addSubview:_springboard];
  }
}

@end
