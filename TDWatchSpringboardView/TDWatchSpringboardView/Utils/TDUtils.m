//
//  TDUtils.m
//  Watch
//
//  Created by timominous on 11/12/14.
//  Copyright (c) 2014 fufulabs. All rights reserved.
//

#import "TDUtils.h"

double TDCGPointDistance(CGPoint p1, CGPoint p2) {
  return sqrt(pow((p1.x - p2.x), 2) + pow((p1.y - p2.y), 2));
}

double TDCGPointSquaredDistance(CGPoint p1, CGPoint p2) {
  return pow(TDCGPointDistance(p1, p2), 2);
}

CGRect TDCGRectWithCenterAndSize(CGPoint center, CGSize size) {
  return CGRectMake(center.x - (size.width * 0.5f), center.y - (size.height * 0.5f), size.width, size.height);
}

CGPoint TDCenterOfCGRect(CGRect rect) {
  return CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
}

CGSize TDCGSizeMultiply(CGSize size, CGFloat widthFactor, CGFloat heightFactor) {
  return CGSizeMake(size.width * widthFactor, size.height * heightFactor);
}

CGSize TDCGSizeDivide(CGSize size, CGFloat widthFactor, CGFloat heightFactor) {
  return CGSizeMake(size.width / widthFactor, size.height / heightFactor);
}

CGPoint TDCGPointMultiply(CGPoint point, CGFloat xFactor, CGFloat yFactor) {
  return CGPointMake(point.x * xFactor, point.y * yFactor);
}

CGPoint TDCGPointDivide(CGPoint point, CGFloat xFactor, CGFloat yFactor) {
  return CGPointMake(point.x / xFactor, point.y / yFactor);
}
