//
//  TDUtils.h
//  Watch
//
//  Created by timominous on 11/12/14.
//  Copyright (c) 2014 fufulabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

double TDCGPointDistance(CGPoint p1, CGPoint p2);
double TDCGPointSquaredDistance(CGPoint p1, CGPoint p2);

CGRect TDCGRectWithCenterAndSize(CGPoint center, CGSize size);

CGSize TDCGSizeMultiply(CGSize size, CGFloat widthFactor, CGFloat heightFactor);
CGSize TDCGSizeDivide(CGSize size, CGFloat widthFactor, CGFloat heightFactor);

CGPoint TDCenterOfCGRect(CGRect rect);
CGPoint TDCGPointMultiply(CGPoint point, CGFloat xFactor, CGFloat yFactor);
CGPoint TDCGPointDivide(CGPoint point, CGFloat xFactor, CGFloat yFactor);