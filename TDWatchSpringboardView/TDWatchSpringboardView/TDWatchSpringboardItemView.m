//
//  TDWatchSpringboardItemView.m
//  Watch
//
//  Created by timominous on 11/12/14.
//  Copyright (c) 2014 fufulabs. All rights reserved.
//

#import "TDWatchSpringboardItemView.h"

static CGFloat const kTDWatchSpringboardMinThreshold = 0.8f;

@interface TDWatchSpringboardItemView ()

@end

@implementation TDWatchSpringboardItemView

- (instancetype)initWithTitle:(NSString *)title image:(UIImage *)image {
  if ((self = [super init])) {
    _title = title;
    _image = image;
    [self onInit];
  }
  return self;
}

- (instancetype)init {
  if ((self = [super init]))
    [self onInit];
  return self;
}

- (void)onInit {
  self.scale = 1.f;
  
  _label = [UILabel.alloc init];
  _label.textColor = UIColor.whiteColor;
  _label.text = _title;
  _label.font = [UIFont systemFontOfSize:[UIFont smallSystemFontSize]];
  [self addSubview:_label];
  
  _icon = [UIImageView.alloc init];
  _icon.image = _image;
  [self addSubview:_icon];
}

- (void)layoutSubviews {
  [super layoutSubviews];
  
  CGSize size = self.bounds.size;
  
  self.icon.center = CGPointMake(size.width * 0.5f, size.height * 0.5f);
  self.icon.bounds = CGRectMake(0.f, 0.f, size.width, size.height);
  
  [self.label sizeToFit];
  self.label.center = CGPointMake(size.width * 0.5f, size.height + 4.f);
  
  CGFloat scale = 60.f / size.width;
  self.icon.transform = CGAffineTransformMakeScale(scale, scale);
}

#pragma mark - Accessors

- (void)setTitle:(NSString *)title {
  _title = title;
  self.label.text = title;
  [self setNeedsLayout];
}

- (void)setImage:(UIImage *)image {
  _image = image;
  self.icon.image = image;
  [self setNeedsLayout];
}

- (void)setScale:(CGFloat)scale {
  [self setScale:scale animated:NO];
}

- (void)setScale:(CGFloat)scale animated:(BOOL)animated {
  if (_scale != scale) {
    BOOL wasSmall = (_scale < kTDWatchSpringboardMinThreshold);
    _scale = scale;
    [self setNeedsLayout];
    
    if ((scale < kTDWatchSpringboardMinThreshold) != wasSmall) {
      if (animated) {
        [UIView animateWithDuration:0.3f animations:^{
          if (scale < kTDWatchSpringboardMinThreshold)
            self.label.alpha = 0.f;
          else
            self.label.alpha = 1.f;
        }];
      } else {
        if (scale < kTDWatchSpringboardMinThreshold)
          self.label.alpha = 0.f;
        else
          self.label.alpha = 1.f;
      }
    }
  }
}

@end
