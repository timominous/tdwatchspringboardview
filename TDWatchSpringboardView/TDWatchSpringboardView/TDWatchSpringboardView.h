//
//  TDWatchSpringboardView.h
//  Watch
//
//  Created by timominous on 11/12/14.
//  Copyright (c) 2014 fufulabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TDWatchSpringboardView : UIScrollView

@property (copy, nonatomic) NSArray *itemViews;
@property (nonatomic) NSUInteger itemDiameter;
@property (nonatomic) NSUInteger itemPadding;
@property (nonatomic) CGFloat minimumItemScaling;

- (void)showAllItemViewsAnimated:(BOOL)animated;
- (NSUInteger)indexOFItemClosestToPoint:(CGPoint)point;
- (void)centerOnIndex:(NSUInteger)index zoomScale:(CGFloat)scale animated:(BOOL)animated;

@end
