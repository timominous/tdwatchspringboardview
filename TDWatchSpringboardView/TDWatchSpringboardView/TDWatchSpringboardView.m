//
//  TDWatchSpringboardView.m
//  Watch
//
//  Created by timominous on 11/12/14.
//  Copyright (c) 2014 fufulabs. All rights reserved.
//

#import "TDWatchSpringboardView.h"

#import "TDWatchSpringboardItemView.h"

#import "TDUtils.h"

@interface TDWatchSpringboardView () <UIScrollViewDelegate>

@property (strong, nonatomic) UIView *touchView;
@property (strong, nonatomic) UIView *contentView;

@property (nonatomic) CGFloat transformFactor;

@property (nonatomic) NSUInteger previousFocusedIndex;
@property (nonatomic) CGFloat previousZoomScale;
@property (nonatomic) CGPoint previousOffset;
@property (nonatomic) CGAffineTransform minTransform;

@property (nonatomic) BOOL minimumZoomLevelIsDirty;
@property (nonatomic) BOOL contentSizeIsDirty;
@property (nonatomic) CGSize contentSizeUnscaled;
@property (nonatomic) CGSize contentSizeExtra;

@property (nonatomic) BOOL centerOnEndDrag;
@property (nonatomic) BOOL centerOnEndDeccelerating;

@property (strong, nonatomic) UIImage *bgView;

@end

@implementation TDWatchSpringboardView

- (instancetype)init {
  if ((self = [super init]))
    [self onInit];
  return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
  if ((self = [super initWithFrame:frame]))
    [self onInit];
  return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
  if ((self = [super initWithCoder:aDecoder]))
    [self onInit];
  return self;
}

- (void)onInit {
  self.delaysContentTouches = NO;
  self.showsHorizontalScrollIndicator = NO;
  self.showsVerticalScrollIndicator = NO;
  self.alwaysBounceHorizontal = YES;
  self.alwaysBounceVertical = YES;
  self.bouncesZoom = YES;
  self.decelerationRate = UIScrollViewDecelerationRateFast;
  self.delegate = self;
  
  self.itemDiameter = 68.f;
  self.itemPadding = 48.f;
  self.minimumItemScaling = 0.5f;
  
  _transformFactor = 1.f;
  _previousZoomScale = self.zoomScale;
  
  _touchView = [UIView.alloc init];
  [self addSubview:_touchView];
  
  _contentView = [UIView.alloc init];
  [self addSubview:_contentView];
}

- (void)setBounds:(CGRect)bounds {
  if (!CGSizeEqualToSize(bounds.size, self.bounds.size))
    [self flagMinimumZoomLevelAsDirty];
  [super setBounds:bounds];
}

- (void)setFrame:(CGRect)frame {
  if (!CGSizeEqualToSize(frame.size, self.bounds.size))
    [self flagMinimumZoomLevelAsDirty];
  [super setFrame:frame];
}

- (void)layoutSubviews {
  [super layoutSubviews];
  
  CGSize size = self.bounds.size;
  UIEdgeInsets insets = self.contentInset;
  
  NSUInteger itemDiameter = self.itemDiameter;
  NSUInteger itemPadding = self.itemPadding;
  
  NSUInteger itemsPerLine = ceil(MIN(size.width, size.height) / MAX(size.width, size.height) * sqrt(self.itemViews.count));
  if (itemsPerLine % 2 == 0)
    itemsPerLine++;
  NSUInteger lines = ceil(self.itemViews.count/(double)itemsPerLine);
  CGFloat newMinimumZoomScale = 0.f;
  
  if (self.contentSizeIsDirty) {
    self.contentSizeUnscaled = CGSizeMake(itemsPerLine * itemDiameter + (itemsPerLine + 1) * itemPadding + (itemDiameter + itemPadding) / 2.f,
                                          lines * itemDiameter + (itemPadding * 2.f));
    newMinimumZoomScale = MIN((size.width - (insets.left + insets.right)) / self.contentSizeUnscaled.width,
                              (size.height - (insets.top + insets.bottom) / self.contentSizeUnscaled.height));
    self.contentSizeExtra = CGSizeMake((size.width - itemDiameter * 0.5f) / newMinimumZoomScale,
                                       (size.height - itemDiameter * 0.5f) / newMinimumZoomScale);
    self.contentSizeUnscaled = CGSizeMake(self.contentSizeUnscaled.width + self.contentSizeExtra.width,
                                          self.contentSizeUnscaled.height + self.contentSizeExtra.height);
    self.contentView.bounds = CGRectMake(0.f, 0.f, self.contentSizeUnscaled.width, self.contentSizeUnscaled.height);
  }
  
  if (self.minimumZoomLevelIsDirty) {
    self.minimumZoomScale = newMinimumZoomScale;
    CGFloat newZoom = MAX(self.zoomScale, newMinimumZoomScale);
    self.zoomScale = newZoom;
    self.previousZoomScale = newZoom;
    
    self.contentView.center = CGPointMake(self.contentSizeUnscaled.width * 0.5f * newZoom,
                                          self.contentSizeUnscaled.height * 0.5f * newZoom);
  }
  
  if (self.contentSizeIsDirty) {
    NSUInteger count = self.itemViews.count;
    for (NSUInteger i = 0; i < count; i++) {
      UIView *view = self.itemViews[i];
      view.bounds = CGRectMake(0.f, 0.f, itemDiameter, itemDiameter);
      
      NSInteger x, y;
      
      NSUInteger line = i / itemsPerLine;
      NSUInteger lineIndex = i % itemsPerLine;
      
      if (i == 0) {
        // This places the first item at the center of the grid
        line = count / itemsPerLine / 2;
        lineIndex = itemsPerLine / 2;
      } else {
        if (line == count / itemsPerLine / 2 && lineIndex == itemsPerLine / 2) {
          // avoid overlapping of icons at the center of the grid
          line = 0;
          lineIndex = 0;
        }
      }
      
      NSInteger offset = (line % 2 == 1) ? (itemDiameter + itemPadding) / 2 : 0;
      x = (self.contentSizeExtra.width * 0.5f) + itemPadding + offset + (lineIndex * (itemPadding + itemDiameter)) + (itemDiameter / 2);
      y = (self.contentSizeExtra.height * 0.5f) + itemDiameter + (line * itemDiameter) + (itemDiameter / 2);
      view.center = CGPointMake(x, y);
    }
    
    _contentSizeIsDirty = NO;
  }
  
  if (self.minimumZoomLevelIsDirty) {
    if (self.previousFocusedIndex <= self.itemViews.count)
      [self centerOnIndex:self.previousFocusedIndex zoomScale:self.previousZoomScale animated:NO];
    
    _minimumZoomLevelIsDirty = NO;
  }
  
  self.previousZoomScale = self.zoomScale;
  self.previousOffset = self.contentOffset;
  
  self.touchView.bounds = CGRectMake(0.f, 0.f,
                                     (self.contentSizeUnscaled.width - self.contentSizeExtra.width) * self.previousZoomScale,
                                     (self.contentSizeUnscaled.height - self.contentSizeExtra.height) * self.previousZoomScale);
  self.touchView.center = CGPointMake(self.contentSizeUnscaled.width * self.previousZoomScale * 0.5f,
                                      self.contentSizeUnscaled.height * self.previousZoomScale * 0.5f);
  
//  [self LM_centerViewIfSmaller];
  
  double scale = MIN(self.minimumItemScaling * self.transformFactor + (1 - self.transformFactor), 1);
  self.minTransform = CGAffineTransformMakeScale(scale, scale);
  for (TDWatchSpringboardItemView *view in self.itemViews) {
    [self transformView:view];
  }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
  self.previousOffset = scrollView.contentOffset;
//  [self repositionRGBView];
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
  self.previousZoomScale = scrollView.zoomScale;
//  [self LM_centerViewIfSmaller];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
  return self.contentView;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
  if (self.centerOnEndDeccelerating) {
    self.centerOnEndDeccelerating = NO;
    [self centerOnIndex:self.previousFocusedIndex zoomScale:self.zoomScale animated:YES];
  }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
  if (self.centerOnEndDrag) {
    self.centerOnEndDrag = NO;
    [self centerOnIndex:self.previousFocusedIndex zoomScale:self.zoomScale animated:YES];
  }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                     withVelocity:(CGPoint)velocity
              targetContentOffset:(inout CGPoint *)targetContentOffset {
  CGSize size = self.bounds.size;
  CGFloat zoomScale = self.zoomScale;
  
  CGPoint proposedTargetCenter = CGPointMake(targetContentOffset->x + (size.width / 2),
                                             targetContentOffset->y + (size.height / 2));
  proposedTargetCenter.x /= zoomScale;
  proposedTargetCenter.y /= zoomScale;
  
  self.previousFocusedIndex = [self closestIndexToPointInContent:proposedTargetCenter];
  UIView *view = self.itemViews[self.previousFocusedIndex];
  CGPoint idealTargetCenter = view.center;
  CGPoint idealTargetOffset = CGPointMake(idealTargetCenter.x - size.width / 2 / zoomScale,
                                          idealTargetCenter.y - size.height / 2 / zoomScale);
  CGPoint correctTargetOffset = CGPointMake(idealTargetOffset.x * zoomScale,
                                            idealTargetOffset.y * zoomScale);
  
  CGPoint currentCenter = CGPointMake(self.contentOffset.x + size.width / 2,
                                      self.contentOffset.y + size.height / 2);
  currentCenter.x /= zoomScale;
  currentCenter.y /= zoomScale;
  
  CGPoint contentCenter = self.contentView.center;
  contentCenter.x /= zoomScale;
  contentCenter.y /= zoomScale;
  CGSize contentSizeNoExtras = CGSizeMake(self.contentSizeUnscaled.width - self.contentSizeExtra.width,
                                          self.contentSizeUnscaled.height - self.contentSizeExtra.height);
  CGRect contentFrame = CGRectMake(contentCenter.x - contentSizeNoExtras.width * 0.5,
                                   contentCenter.y - contentSizeNoExtras.height * 0.5f,
                                   contentSizeNoExtras.width, contentSizeNoExtras.height);
  
  if (!CGRectContainsPoint(contentFrame, proposedTargetCenter)) {
    if (!CGRectContainsPoint(contentFrame, currentCenter)) {
      *targetContentOffset = self.contentOffset;
      _centerOnEndDrag = YES;
      return;
    } else {
      float priority = 0.8;
      *targetContentOffset = CGPointMake(targetContentOffset->x * (1 - priority) + correctTargetOffset.x * priority,
                                         targetContentOffset->y * (1 - priority) + correctTargetOffset.y * priority);
      _centerOnEndDeccelerating = YES;
      return;
    }
  }
  
  *targetContentOffset = correctTargetOffset;
}

#pragma mark - Actions

- (CGRect)insertRectInSelf{
  UIEdgeInsets insets = self.contentInset;
  CGSize size = self.bounds.size;
  return CGRectMake(insets.left, insets.top,
                    size.width - (insets.left + insets.right),
                    size.height - (insets.top + insets.bottom));
}

- (CGRect)fullContentRectInContentSpace {
  return CGRectMake(self.contentSizeExtra.width * 0.5f,
                    self.contentSizeExtra.height * 0.5f,
                    self.contentSizeUnscaled.width - self.contentSizeExtra.width,
                    self.contentSizeUnscaled.height - self.contentSizeExtra.height);
}

- (void)LcenterOnClosestToScreenCenterAnimated:(BOOL)animated {
  CGSize size = self.bounds.size;
  CGPoint center = CGPointMake(size.width * 0.5f, size.width * 0.5f);
  NSUInteger closestIndex = [self closestIndexToPointInSelf:center];
  [self centerOnIndex:closestIndex zoomScale:self.zoomScale animated:animated];
}

- (NSUInteger)closestIndexToPointInContent:(CGPoint)point {
  BOOL hasItem = NO;
  CGFloat distance = 0.f;
  NSUInteger index = self.previousFocusedIndex;
  for (NSUInteger i = 0; i < self.itemViews.count; i++) {
    UIView *view = self.itemViews[i];
    CGPoint center = view.center;
    double potentialDistance = TDCGPointDistance(center, point);
    
    if (potentialDistance < distance || !hasItem) {
      hasItem = YES;
      distance = potentialDistance;
      index = i;
    }
  }
  
  return index;
}

- (NSUInteger)closestIndexToPointInSelf:(CGPoint)point {
  CGPoint pointInContent = [self convertPointFromSelfToContent:point];
  return [self closestIndexToPointInContent:pointInContent];
}

- (void)didTapContentView:(UITapGestureRecognizer *)sender {
  CGFloat maximumZoom = 1.f;
  CGPoint loc = [sender locationInView:self];
  NSUInteger index = [self closestIndexToPointInSelf:loc];
  
  if (self.zoomScale != self.minimumZoomScale) {
    [self showAllItemViewsAnimated:YES];
  } else {
    [UIView animateWithDuration:0.5f animations:^{
      [self centerOnIndex:index zoomScale:maximumZoom animated:NO];
      [self layoutIfNeeded];
    } completion:nil];
  }
}

- (void)flagMinimumZoomLevelAsDirty {
  _minimumZoomLevelIsDirty = YES;
  _contentSizeIsDirty = YES;
  [self setNeedsLayout];
}

- (void)flagContentSizeAsDirty {
  _contentSizeIsDirty = YES;
  [self flagMinimumZoomLevelAsDirty];
}

- (void)transformView:(TDWatchSpringboardItemView *)view {
  CGSize size = self.bounds.size;
  CGFloat zoomScale = self.previousZoomScale;
  UIEdgeInsets insets = self.contentInset;
  CGPoint contentOffset = self.contentOffset;
  
  CGPoint center = view.center;
  CGRect frame = [self convertRect:CGRectMake(view.center.x - self.itemDiameter / 2,
                                              view.center.y - self.itemDiameter / 2,
                                              self.itemDiameter, self.itemDiameter)
                          fromView:view.superview];
  frame.origin.x -= contentOffset.x;
  frame.origin.y -= contentOffset.y;
  center = CGPointMake(frame.origin.x + frame.size.width / 2, frame.origin.y + frame.size.height / 2);
  NSUInteger padding = self.itemPadding * zoomScale * 0.4f;
  double distanceToBorder = size.width;
  float xOffset = 0;
  float yOffset = 0;
  
  double distanceToBeOffset = self.itemDiameter * zoomScale * (MIN(size.width, size.height) / 320.f);

  float leftDistance = center.x - padding - insets.left;
  if (leftDistance < distanceToBeOffset) {
    if (leftDistance < distanceToBorder)
      distanceToBorder = leftDistance;
    xOffset = 1 - (leftDistance / distanceToBeOffset);
  }
  
  float topDistance = center.y - padding - insets.top;
  if (topDistance < distanceToBeOffset) {
    if (topDistance < distanceToBorder)
      distanceToBorder = topDistance;
    yOffset = 1 - (topDistance / distanceToBeOffset);
  }
  
  float rightDistance = size.width - padding - center.x - insets.right;
  if (rightDistance < distanceToBeOffset) {
    if (rightDistance < distanceToBorder)
      distanceToBorder = rightDistance;
    xOffset = -(1 - (rightDistance / distanceToBeOffset));
  }
  
  float bottomDistance = size.height - padding - center.y - insets.bottom;
  if (bottomDistance < distanceToBeOffset) {
    if (bottomDistance < distanceToBorder)
      distanceToBorder = bottomDistance;
    xOffset = -(1 - (bottomDistance / distanceToBeOffset));
  }
  
  distanceToBorder *= 2;
  double usedScale;
  if (distanceToBorder < distanceToBeOffset * 2) {
    if (distanceToBorder < -((NSInteger)(self.itemDiameter * 2.5))) {
      view.transform = self.minTransform;
      usedScale = self.minimumItemScaling * zoomScale;
    } else {
      double rawScale = MAX(distanceToBorder / (distanceToBeOffset * 2), 0);
      rawScale = MIN(rawScale, 1);
//      rawScale = 1 - pow(1 - rawScale, 2);
      rawScale = rawScale * (2 - rawScale);
      double scale = rawScale * (1 - self.minimumItemScaling) + self.minimumItemScaling;

      xOffset = frame.size.width * 0.8f * (1 - rawScale) * xOffset;
      yOffset = frame.size.width * 0.5f * (1 - rawScale) * yOffset;
      
      float translationModifer = MIN(distanceToBorder / self.itemDiameter + 2.5f, 1.f);
      
      scale = MAX(MIN(scale * self.transformFactor + (1 - self.transformFactor), 1), 0);
      translationModifer = MIN(translationModifer * self.transformFactor, 1);
      view.transform = CGAffineTransformTranslate(CGAffineTransformMakeScale(scale, scale),
                                                  xOffset * translationModifer,
                                                  yOffset * translationModifer);
      usedScale = scale * zoomScale;
    }
  } else {
    view.transform = CGAffineTransformIdentity;
    usedScale = zoomScale;
  }
  
  if (self.isDragging || self.isZooming)
    [view setScale:usedScale animated:YES];
  else
    view.scale = usedScale;
}

- (CGSize)convertSizeFromContentToSelf:(CGSize)size {
  return TDCGSizeMultiply(size, self.zoomScale, self.zoomScale);
}

- (CGSize)convertSizeFromSelfToContent:(CGSize)size {
  return TDCGSizeDivide(size, self.zoomScale, self.zoomScale);
}

- (CGPoint)convertPointFromContentToSelf:(CGPoint)point {
  return TDCGPointMultiply(point, self.zoomScale, self.zoomScale);
}

- (CGPoint)convertPointFromSelfToContent:(CGPoint)point {
  return TDCGPointDivide(point, self.zoomScale, self.zoomScale);
}

#pragma mark - Accessors

- (void)setItemViews:(NSArray *)itemViews {
  if (_itemViews != itemViews) {
    for (UIView *view in _itemViews) {
      if ([view isDescendantOfView:self])
        [view removeFromSuperview];
    }
      
    _itemViews = [itemViews copy];
    
    for (UIView *view in _itemViews)
      [self.contentView addSubview:view];
    
    [self flagContentSizeAsDirty];
  }
}

- (void)setItemDiameter:(NSUInteger)itemDiameter {
  if (_itemDiameter != itemDiameter) {
    _itemDiameter = itemDiameter;
    [self flagContentSizeAsDirty];
  }
}

- (void)setItemPadding:(NSUInteger)itemPadding {
  if (_itemPadding != itemPadding) {
    _itemPadding = itemPadding;
    [self flagContentSizeAsDirty];
  }
}

- (void)setMinimumItemScaling:(CGFloat)minimumItemScaling {
  if (_minimumItemScaling != minimumItemScaling) {
    _minimumItemScaling = minimumItemScaling;
    [self setNeedsLayout];
  }
}

- (void)showAllItemViewsAnimated:(BOOL)animated {
  CGRect contentRect = [self fullContentRectInContentSpace];
  self.previousFocusedIndex = [self closestIndexToPointInContent:TDCenterOfCGRect(contentRect)];
  
  if (animated) {
    UIViewAnimationOptions opts = UIViewAnimationOptionLayoutSubviews | UIViewAnimationOptionAllowAnimatedContent |
    UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseInOut;
    [UIView animateWithDuration:0.5f delay:0.f options:opts animations:^{
      [self zoomToRect:contentRect animated:NO];
      [self layoutIfNeeded];
    } completion:nil];
  } else {
    [self zoomToRect:contentRect animated:NO];
  }
}

- (NSUInteger)indexOFItemClosestToPoint:(CGPoint)point {
  return [self closestIndexToPointInSelf:point];
}

- (void)centerOnIndex:(NSUInteger)index zoomScale:(CGFloat)scale animated:(BOOL)animated {
  self.previousFocusedIndex = index;
  UIView *view = self.itemViews[index];
  CGPoint center = view.center;
  
  if (scale != self.zoomScale) {
    CGRect rect = TDCGRectWithCenterAndSize(center, view.bounds.size);
    [self zoomToRect:rect animated:animated];
  } else {
    CGSize size = self.bounds.size;
    CGPoint centerInSelf = [self convertPointFromContentToSelf:center];
    CGRect rect = TDCGRectWithCenterAndSize(centerInSelf, size);
    [self scrollRectToVisible:rect animated:animated];
  }
}

@end
