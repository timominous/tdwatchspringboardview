//
//  TDWatchSpringboardItemView.h
//  Watch
//
//  Created by timominous on 11/12/14.
//  Copyright (c) 2014 fufulabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TDWatchSpringboardItemView : UIView

@property (strong, readonly) UIImageView *icon;
@property (strong, readonly) UILabel *label;
@property (nonatomic) CGFloat scale;

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) UIImage *image;

- (void)setScale:(CGFloat)scale animated:(BOOL)animated;

- (instancetype)initWithTitle:(NSString *)title image:(UIImage *)image;

@end
