Pod::Spec.new do |s|
  s.name    = 'TDWatchSpringboardView'
  s.version = '0.0.1'
  s.license = { :type => 'MIT', :file => 'LICENSE' }
  s.summary = 'Reusable Apple Watch UI prototype view'
  s.homepage = 'https://timominous@bitbucket.org/timominous/tdwatchspringboardview.git'
  s.authors = {
    'timominous' => 'timominous@gmail.com'
  }

  s.source = {
    :git => 'https://timominous@bitbucket.org/timominous/tdwatchspringboardview.git',
    :tag => "#{s.version}"
  }
  s.platform = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'TDWatchSpringboardView/TDWatchSpringboardView/**/*.{h,m}'
  s.preserve_paths = '*'
end
